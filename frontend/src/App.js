import React from "react";
import AssetsList from "./components/assetsList";
import "./App.css";

function App() {

  return (
    <div className="App">
      <AssetsList />
    </div>
  );
}

export default App;
