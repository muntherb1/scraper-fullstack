import http from "../http-common";

const getAll = (params) => {

  return http.get("/scrape", { params });
};

const removeAll = () => {

  return http.delete("/scrape")
}

export {getAll, removeAll};