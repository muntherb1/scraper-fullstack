import React, { useState, useEffect } from "react";
import {getAll, removeAll} from "../services/scraperServices";
import Pagination from "@material-ui/lab/Pagination";
import PhotoAlbum from "react-photo-album";

const AssetsList = () => {
    const [assets, setAssets] = useState([]);
    const [searchTitle, setSearchTitle] = useState("");

    const [page, setPage] = useState(1);
    const [count, setCount] = useState(0);
    const [pageSize, setPageSize] = useState(5);
    const [type, setType] = useState('')

    const pageSizes = [5, 6, 7, 8, 9, 10, 15, 20, 25, 30, 35, 40];
    const types = {Image: 'IMG', Video: 'SOURCE', Any: ''}

    const onChangeSearchTitle = (e) => {
        const searchTitle = e.target.value;
        setSearchTitle(searchTitle);
    };

    const getRequestParams = (searchTitle, page, pageSize, type) => {
        let params = {};

        if (searchTitle) {
            params["title"] = searchTitle;
        }

        if (page) {
            params["page"] = page - 1;
        }

        if (pageSize) {
            params["size"] = pageSize;
        }

        if (type) {
            params["type"] = type
        }

        return params;
    };

    const retrieveAssets = () => {
        const params = getRequestParams(searchTitle, page, pageSize, type);

        getAll(params)
            .then((response) => {
                const { scrapedAssets, totalPages } = response.data;

                setAssets(scrapedAssets);
                setCount(totalPages);

                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    };

    useEffect(retrieveAssets, [page, pageSize, type]);

    const refreshList = () => {
        retrieveAssets();
      };


    const removeAllAssets = () => {
        removeAll()
          .then((response) => {
            console.log(response.data);
            refreshList();
          })
          .catch((e) => {
            console.log(e);
          });
    };

    const handlePageChange = (event, value) => {
        setPage(value);
    };

    const handlePageSizeChange = (event) => {
        setPageSize(event.target.value);
        setPage(1);
    };

    const handleTypeChange = (event) => {
        setType(event.target.value);
    }

    return (
        <div className="list row">
            <div className="col-md-8" style={{ margin: 20 }}>
                <div className="input-group mb-3">
                    <input
                        style={{ margin: 10, width: 400 }}
                        type="text"
                        className="form-control"
                        placeholder="Search by title"
                        value={searchTitle}
                        onChange={onChangeSearchTitle}
                    />
                    <div className="input-group-append">
                        <button
                            style={{ width: 100 }}
                            className="btn btn-outline-secondary"
                            type="button"
                            onClick={retrieveAssets}
                        >
                            Search
                        </button>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <h4>Assets List</h4>

                <div className="mt-3" style={{ margin: 25 }}>
                    {"Items per Page: "}
                    <select onChange={handlePageSizeChange} value={pageSize} style={{marginRight: 100}}>
                        {pageSizes.map((size) => (
                            <option key={size} value={size}>
                                {size}
                            </option>
                        ))}
                    </select>
                    {"Type: "}
                    <select onChange={handleTypeChange} value={type} style={{marginLeft: 10}}>
                        {Object.entries(types).map(([key,value]) => (
                            <option key={key} value={value}>
                                {key}
                            </option>
                        ))}
                    </select>

                    <Pagination
                        style={{ margin: 25, display: 'flex', justifyContent: 'center' }}
                        className="my-3"
                        count={count}
                        page={page}
                        siblingCount={1}
                        boundaryCount={1}
                        variant="outlined"
                        shape="rounded"
                        onChange={handlePageChange}
                    />
                </div>

                {console.log(assets)}
                {type === 'SOURCE' ? assets.filter(src=> src.type==='SOURCE').map((vid)=> <video autoplay controls width={vid.width} height={vid.height}><source src={vid.src} type="video/mp4"></source></video>) :
                <PhotoAlbum key={assets.id} layout="rows" photos={assets} />}
                <button
                    style={{ margin: 20 }}
                    className="m-3 btn btn-sm btn-danger"
                    onClick={removeAllAssets}
                >
                    Remove All
                </button>
            </div>
        </div>
    );
};

export default AssetsList;