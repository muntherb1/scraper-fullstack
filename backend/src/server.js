// optimized for Docker image

const express = require("express");
const morgan = require("morgan");
const createError = require('http-errors');
const scrapingRouter = require('./routes/scraper')
const basicAuth = require('express-basic-auth');
const sequelize = require("./database");

// Api
const app = express();
app.use(morgan("common"));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//Basic Auth
app.use(basicAuth({
  users: {
    'admin': 'admin'
  },
  challenge: true,
  unauthorizedResponse: 'Incorrect Basic Auth username or password.'
}))


try {
  sequelize.sync({});
  console.log('DB Connection has been established successfully.');
} catch (error) {
  console.error('Unable to connect to the database:', error);
}

app.use('/scrape', scrapingRouter)


app.get("/healthz", function (req, res) {
  // do app logic here to determine if app is truly healthy
  // you should return 200 if healthy, and anything else will fail
  // if you want, you should be able to restrict this to localhost (include ipv4 and ipv6)
  res.send("I am happy and healthy\n");
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
});
module.exports = app;