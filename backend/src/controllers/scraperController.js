var puppeteer = require('puppeteer');
var Urls = require('../models/urlsModels');
var { Op } = require('sequelize');

exports.scrapeData = async(req, res, next) => {
    let arrayOfURLs = req.body
    if(!arrayOfURLs || arrayOfURLs.length === 0) throw new Error('Please add at least one URL to scrape');
    arrayOfURLs.filter(src => src !== '');

    const scrapeImages = async (websiteURL) => {
        try {
        const browser = await puppeteer.connect({
             browserWSEndpoint: 'ws://browserless:3000/' 
        });
        const page = await browser.newPage();
        await page.goto(websiteURL);
        await page.waitForFunction(() => 
        document.querySelectorAll('img, source[type="video/mp4"], source[type="video/webm"], source[type="video/ogg"]').length
        );

        let data = await page.evaluate( () => {

            const images = document.querySelectorAll('img, source[type="video/mp4"], source[type="video/webm"], source[type="video/ogg"]');

            const urls = Array.from(images).flatMap(({src, nodeName}) => !src || src === '' ? []: {src, type: nodeName, width: innerWidth, height: innerHeight});

            return urls;
        })
        await page.screenshot({ path: 'ss.png'})
        await browser.close();

        return data;
            
        } catch (error) {
            console.log(error)
            throw new Error(error)
        }
        
    }
    
    const data = await Promise.all(
        arrayOfURLs.map(async (website) => {
            return await scrapeImages(website)
        })
    )

    try {
        await Urls.bulkCreate(data.flat())
        } 
        catch (error) {
           console.log(error) 
    }

    const datafromDB = await Urls.findAll();
    res.send(datafromDB)
}

exports.getData = async(req, res) => {

    const getPagination = (page, size) => {
        const limit = size ? +size : 5;
        const offset = page ? page * limit : 0;
        return { limit, offset };
    };

    const getPagingData = (data, page, limit) => {
        const { count: totalAssets, rows: scrapedAssets } = data;
        const currentPage = page ? +page : 0;
        const totalPages = Math.ceil(totalAssets / limit);
        return { totalAssets, scrapedAssets, totalPages, currentPage };
    };

    const { page, size, title, type } = req.query;
    var condition = title ? { src: { [Op.like]: `%${title}%` } } : null;
    type ? condition = {...condition, type: {[Op.eq]: type} } : '';
    const { limit, offset } = getPagination(page, size);



    const datafromDB = await Urls.findAndCountAll({ where: condition, limit, offset });
    const response = getPagingData(datafromDB, page, limit);
    res.send(response)
}

exports.deleteAll = (req, res) => {
    Urls.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} URLS were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all URLS."
        });
      });
  };
