const { Sequelize } = require('sequelize')
const { database: {database, user, password} } = require('./config');


const sequelize = new Sequelize(database, user, password, {
  dialect: 'mariadb',
  host: 'db'
});

module.exports = sequelize;