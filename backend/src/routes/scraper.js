var express = require('express');
var router = express.Router();
const {scrapeData, getData, deleteAll} = require('../controllers/scraperController');

// Post request with URLS
router.get('/', getData)

router.post('/', scrapeData);

router.delete('/', deleteAll)

module.exports = router;