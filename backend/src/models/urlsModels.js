const { DataTypes, Model, Sequelize} = require('sequelize');
const sequelize = require('../database')

class Urls extends Model {}
 
Urls.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    src: DataTypes.STRING(1024),
    type: DataTypes.STRING,
    width: DataTypes.INTEGER,
    height: DataTypes.INTEGER
  },
  {
    sequelize,
    modelName: "urls",
    timestamps: false
  }
);


module.exports = Urls;

